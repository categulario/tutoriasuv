<?php

use Illuminate\Database\Seeder;
//Provedor//paquete//Carpeta/Entidad<?php

use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Users
        Permission::create([
            'name'          => 'Navegar usuarios',
            'slug'          => 'users.index',
            'description'   => 'Lista y navega todos los usuarios del sistema',
        ]);
        Permission::create([
            'name'          => 'Ver detalle de usuario',
            'slug'          => 'users.show',
            'description'   => 'Ve en detalle cada usuario del sistema',            
        ]);
        
        Permission::create([
            'name'          => 'Edición de usuarios',
            'slug'          => 'users.edit',
            'description'   => 'Podría editar cualquier dato de un usuario del sistema',
        ]);
        
        Permission::create([
            'name'          => 'Eliminar usuario',
            'slug'          => 'users.destroy',
            'description'   => 'Podría eliminar cualquier usuario del sistema',      
        ]);
        //Roles
        Permission::create([
            'name'          => 'Navegar roles',
            'slug'          => 'roles.index',
            'description'   => 'Lista y navega todos los roles del sistema',
        ]);
        Permission::create([
            'name'          => 'Ver detalle de un rol',
            'slug'          => 'roles.show',
            'description'   => 'Ve en detalle cada rol del sistema',            
        ]);
        
        Permission::create([
            'name'          => 'Creación de roles',
            'slug'          => 'roles.create',
            'description'   => 'Podría crear nuevos roles en el sistema',
        ]);
        
        Permission::create([
            'name'          => 'Edición de roles',
            'slug'          => 'roles.edit',
            'description'   => 'Podría editar cualquier dato de un rol del sistema',
        ]);
        
        Permission::create([
            'name'          => 'Eliminar roles',
            'slug'          => 'roles.destroy',
            'description'   => 'Podría eliminar cualquier rol del sistema',      
        ]);
        //Tutorias
        Permission::create([
            'name'          => 'Listar tutorias',
            'slug'          => 'tutorias.index',
            'description'   => 'Lista todas las tutorías registradas',
        ]);

        Permission::create([
            'name'          => 'Ver detalle de una tutoria',
            'slug'          => 'tutorias.show',
            'description'   => 'Ver en detalle cada tutoria del sistema',            
        ]);
            
        Permission::create([
            'name'          => 'Crear tutoría',
            'slug'          => 'tutorias.create',
            'description'   => 'Registrar nueva tutoría',
        ]);
        
        Permission::create([
            'name'          => 'Editar tutoría',
            'slug'          => 'tutorias.edit',
            'description'   => 'Editar tutoría',
        ]);
        
        Permission::create([
            'name'          => 'Eliminar tutoria',
            'slug'          => 'tutorias.destroy',
            'description'   => 'Eliminar tutoría',      
        ]);

        //Evaluaciones
        
        Permission::create([
            'name'          => 'Listar evaluaciones',
            'slug'          => 'evaluaciones.index',
            'description'   => 'Lista las evaluaciones registradas',
        ]);

        Permission::create([
            'name'          => 'Crear evaluacion',
            'slug'          => 'evaluaciones.create',
            'description'   => 'Registrar nueva evaluación',
        ]);
        
        Permission::create([
            'name'          => 'Editar evaluacion',
            'slug'          => 'evaluaciones.edit',
            'description'   => 'Editar evaluacion',
        ]);

        //Programas
        
        Permission::create([
            'name'          => 'Listar Programas',
            'slug'          => 'educativos.index',
            'description'   => 'Lista los programas educativos registrados',
        ]);

        Permission::create([
            'name'          => 'Crear evaluacion',
            'slug'          => 'educativos.create',
            'description'   => 'Registrar nuevo programa educativo',
        ]);
        
        Permission::create([
            'name'          => 'Editar programa educativo',
            'slug'          => 'educativos.edit',
            'description'   => 'Editar programa educativo',
        ]);
        
    }
}