<?php
use App\User; 
use App\ProgramaEducativo; 
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;

//Rol del administrador
use Caffeinated\Shinobi\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Ejecuta el factory de los usuarios
    	factory(App\User::class ,20)->create();	

    	Role::create([
    		'name'		=> 'Admin',
    		'slug'		=> 'admin',
    		'special'	=> 'all-access'
        ]);
        Role::create([
    		'name'		=> 'Maestros',
    		'slug'		=> 'maestros',
        ]);
        Role::create([
    		'name'		=> 'Alumnos',
    		'slug'		=> 'alumnos',
        ]);
        Role::create([
    		'name'		=> 'Coordinadores',
    		'slug'		=> 'coordinadores',
        ]);

        $pe = ProgramaEducativo::create ([
            'nombre'    => 'Redes y Servicios de Computo'
        ]);

        $brenda = User::create([
            'name'		=> 'Brenda',
            'email'     => 'blvh24uv@gmail.com',
            'email_verified_at' => Carbon::now(),
            'password'  => Hash::make('12345678'),
            'programa_educativo_id' => $pe->id,
        ]);

        $brenda->assignRoles('admin','alumnos');
        for ($i = 1; $i<4; $i++){
            $l = User::find($i);
            
            $l->programa_educativo_id = $pe->id;
            $l->save();
            $l->assignRoles('alumnos');

        }

        for ($i = 4; $i<7; $i++){
            User::find($i)->assignRoles('maestros');
        }

        for ($i = 7; $i<10; $i++){
            User::find($i)->assignRoles('coordinadores');
        }
    }
}
