<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Tutoria;
use App\User;
use App\ProgramaEducativo;
use Faker\Generator as Faker;

$factory->define(Tutoria::class, function (Faker $faker) {
    return [
        //Estructura de la tabla con datos aleatorios
        'lugar' => $faker->sentence,
        'fecha' => $faker->dateTime,
        'sesion' => $faker->numberBetween($min = 1, $max = 3),
        'alumno_id' => function () {
            return factory(User::class)->create()->id;
        },
        'maestro_id' => function () {
            return factory(User::class)->create()->id;
        },
        'programa_educativo_id' => function () {
            return factory(ProgramaEducativo::class)->create()->id;
        }
    ];
});
