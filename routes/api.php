<?php

use Illuminate\Http\Request;
use Caffeinated\Shinobi\Models\Role;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/alumnos/{programa_educativo}', function (Request $request, $id) {
    
    $rolAlumno = Role::where('name', 'Alumnos')->first();

    return $rolAlumno->users()->where('programa_educativo_id',$id)->get();
});
