<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Restricciones a nivel de rutas

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Registro de permisos
//Las rutas van estrictamente relacionadas con los permisos

//El middleware auth verifica si el usuario esta autenticado

Route::middleware(['auth'])->group(function() {
	//Roles (//Ruta. controlador/Nombre de la ruta /Permiso)
	Route::post('roles/store', 'RoleController@store')->name('roles.store')
		->middleware('permission:roles.create');


	Route::get('roles', 'RoleController@index')->name('roles.index')
		->middleware('permission:roles.create');


	Route::get('roles/create', 'RoleController@create')->name('roles.create')
		->middleware('permission:roles.create');


	Route::put('roles/{role}', 'RoleController@update')->name('roles.update')
		->middleware('permission:roles.edit');


	Route::get('roles/{role}', 'RoleController@show')->name('roles.show')
		->middleware('permission:roles.show');


	Route::delete('roles/{role}', 'RoleController@destroy')->name('roles.destroy')
		->middleware('permission:roles.destroy');


	Route::get('roles/{role}/edit', 'RoleController@edit')->name('roles.edit')
		->middleware('permission:roles.edit');

	//Programas Educativos

	Route::post('educativos/store', 'EducativoController@store')->name('educativos.store')
		->middleware('permission:roles.create');


	Route::get('educativos', 'EducativoController@index')->name('educativos.index')
		->middleware('permission:educativos.create');


	Route::get('educativos/create', 'EducativoController@create')->name('educativos.create')
		->middleware('permission:educativos.create');


	Route::put('educativos/{educativo}', 'EducativoController@update')->name('educativos.update')
		->middleware('permission:educativos.edit');


	Route::get('educativos/{educativo}', 'EducativoController@show')->name('educativos.show')
		->middleware('permission:educativos.show');


	Route::delete('educativos/{educativo}', 'EducativoController@destroy')->name('educativos.destroy')
		->middleware('permission:educativos.destroy');


	Route::get('educativos/{educativo}/edit', 'EducativoController@edit')->name('educativos.edit')
		->middleware('permission:educativos.edit');


	//Tutorías

	Route::post('tutorias/store', 'TutoriaController@store')->name('tutorias.store')
		->middleware('permission:roles.create');


	Route::get('tutorias', 'TutoriaController@index')->name('tutorias.index')
		->middleware('permission:tutorias.create');


	Route::get('tutorias/create', 'TutoriaController@create')->name('tutorias.create')
		->middleware('permission:tutorias.create');


	Route::put('tutorias/{tutoria}', 'TutoriaController@update')->name('tutorias.update')
		->middleware('permission:tutorias.edit');


	Route::get('tutorias/{tutoria}', 'TutoriaController@show')->name('tutorias.show')
		->middleware('permission:tutorias.show');


	Route::delete('tutorias/{tutoria}', 'TutoriaController@destroy')->name('tutorias.destroy')
		->middleware('permission:tutorias.destroy');


	Route::get('tutorias/{tutoria}/edit', 'TutoriaController@edit')->name('tutorias.edit')
		->middleware('permission:tutorias.edit');

	//Users

	Route::get('users', 'UserController@index')->name('users.index')
		->middleware('permission:users.create');


	Route::get('users/{user}', 'UserController@show')->name('users.show')
		->middleware('permission:users.show');

	Route::put('users/{user}', 'UserController@update')->name('users.update')
		->middleware('permission:users.edit');

	Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy')
		->middleware('permission:users.destroy');


	Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit')
		->middleware('permission:users.edit');
	
	//Evaluaciones

	Route::post('evaluaciones/{tutoria}/store', 'EvaluacionController@store')->name('evaluaciones.store')
		->middleware('permission:roles.create');


	Route::get('evaluaciones', 'EvaluacionController@index')->name('evaluaciones.index')
		->middleware('permission:evaluaciones.create');


	Route::get('evaluaciones/{tutoria}/create', 'EvaluacionController@create')->name('evaluaciones.create')
		->middleware('permission:evaluaciones.create');


	Route::put('evaluaciones/{evaluacion}', 'EvaluacionController@update')->name('evaluaciones.update')
		->middleware('permission:evaluaciones.edit');


	Route::get('evaluaciones/{evaluacion}', 'EvaluacionController@show')->name('evaluaciones.show')
		->middleware('permission:evaluaciones.show');

	Route::get('evaluaciones/{evaluacion}/edit', 'EvaluacionController@edit')->name('evaluaciones.edit')
		->middleware('permission:evaluaciones.edit');

});
