<div class="form-group">
	{{ Form::label('descripcion', 'Notas de la evaluación') }}
	{{ Form::text('descripcion', null, ['class' => 'form-control', 'id' => 'descripcion']) }}
</div>
<div class="form-group">
	{{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
</div>