<div class="form-group">
	{{ Form::label('lugar', 'Salón o cubículo donde se dará la tutoría') }}
	{{ Form::text('lugar', null, ['class' => 'form-control', 'id' => 'lugar']) }}
</div>
<div class="form-group">
	{{ Form::label('fecha', 'Fecha de la tutoría') }}
	{{ Form::date('fecha', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
	{{ Form::label('maestro_id', 'Nombre del maestro que impartirá la tutoría') }}
	{{ Form::select('maestro_id', $maestros) }}
</div>
<div class="form-group">
	{{ Form::label('sesion', 'Número de tutoría') }}
	{{ Form::number('sesion', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
	{{ Form::label('programa_educativo_id', 'Programa Educativo') }}
	{{ Form::select('programa_educativo_id', $programa) }}
</div>
<div class="form-group">
	{{ Form::label('alumno_id', 'Alumno') }}
	{{ Form::select('alumno_id', $alumnos) }}

	<script type="text/javascript">
		document.addEventListener('DOMContentLoaded', function () {
 
			//Select del programa educativo, change cambia el valor.
			document.getElementById('programa_educativo_id').addEventListener('change', function (event) {
				//La funcion fetch hace una peticion a una ruta y obtiene la respuesta (string)
				fetch(`/api/alumnos/${event.target.value}`)
					//y los parsea
					.then(body => body.json())
					//los guarda en la variable data
					.then((data) => {
						//Representa el select del alumno
						const alumnosSelect = document.getElementById('alumno_id');
	
						// vaciar el select
						while (alumnosSelect.hasChildNodes()) {
							alumnosSelect.removeChild(alumnosSelect.lastChild);
						}
						//recorre data con la variable alumno
						data.forEach((alumno) => {
							//Crea un elemento opcion
							const option = document.createElement('option');
							//valor y texto del option
							option.value = alumno.id;
							option.innerHTML = alumno.name;
							//Agrega los hijos del select que se crearon en option
							alumnosSelect.appendChild(option);
						});
					});
  			});
  		});
	</script>
</div>
<div class="form-group">
	{{ Form::label('temas', 'Temas a tratar por el profesor, este campo no existe, se necesita añadir') }}
</div>
<div class="form-group">
	{{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
</div>