<?php

namespace App\Http\Controllers;

use Caffeinated\Shinobi\Models\Role;
use App\Tutoria;
use App\Evaluacion;
use Illuminate\Http\Request;

class EvaluacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Se obtiene toda la informacion de la entidad evaluaciones para mostrarlas posteriormente pasarlos en una varible y mostrarlos en una vista
        $evaluaciones = Evaluacion::paginate();
        
        return view('evaluaciones.index', compact('evaluaciones'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Tutoria $tutoria)
    {
        if ($tutoria->alumno_id != $request->user()->id){
            return response('Acceso inautorizado', 403);
        }
        $rol = Role::where('name', 'Maestros')->first();
        $rolAlumno = Role::where('name', 'Alumnos')->first();

        $maestros = [];
        $alumnos = [];

        foreach ($rol->users as $user) {
            $maestros[$user['id']] = $user['name'];
        }

        foreach ($rolAlumno->users as $user) {
            $alumnos[$user['id']] = $user['name'];
        }

        return view('evaluaciones.create', compact('maestros', 'alumnos','tutoria'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Tutoria $tutoria)
    {
        if ($tutoria->alumno_id != $request->user()->id){
            return response('Acceso inautorizado', 403);
        }
        //Guarda la tutoría creada en la variable
        $evaluacion = Evaluacion::create(array_merge(['tutoria_id'=>$tutoria->id],$request->all()));

        return redirect()->route('evaluaciones.edit', $evaluacion->id)
            ->with('info', 'Evaluación guardada correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Evaluacion  $evaluacion
     * @return \Illuminate\Http\Response
     */

    //Realiza la búsqueda automaticamente del id de la evaluaciones
    public function show(Evaluacion $evaluacion)
    {
        //dd($evaluacion->id);
        return view('evaluaciones.show', compact('evaluacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Evaluacion  $evaluacion
     * @return \Illuminate\Http\Response
     */
    public function edit(Evaluacion $evaluacion)
    {
        return view('evaluaciones.edit', compact('evaluacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Evaluacion  $evaluacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Evaluacion $evaluacion)
    {
        $evaluacion->update($request->all());

        return redirect()->route('evaluaciones.edit', $evaluacion->id)
            ->with('info', 'Evaluación actualizada correctamente');
    }

}
